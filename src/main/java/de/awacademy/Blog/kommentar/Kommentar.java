package de.awacademy.Blog.kommentar;

import de.awacademy.Blog.beitrag.Beitrag;
import de.awacademy.Blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue
    private Long id;


    private String text;

    private Instant postedAt;

    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private Nutzer nutzer;

    public Kommentar() {
    }

    /**
     *Kommentare weden einem Nutzer und einem Beitrag zugeordnet
     * @param text Inhalt des Kommentares
     * @param postedAt Zeitpunkt der veröffentlichung
     */
    public Kommentar(String text, Nutzer nutzer, Instant postedAt, Beitrag beitrag) {
        this.text = text;
        this.postedAt = postedAt;
        this.nutzer = nutzer;
        this.beitrag = beitrag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }
}
