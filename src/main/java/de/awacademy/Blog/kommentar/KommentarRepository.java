package de.awacademy.Blog.kommentar;

import de.awacademy.Blog.beitrag.Beitrag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KommentarRepository extends CrudRepository<Kommentar, Long> {

    /**
     * Sucht alle Kommentare mit entsprechender BeitragsId
     * @return Kommentarliste
     */
    List<Kommentar> findAllByBeitragId(Long id);

}
