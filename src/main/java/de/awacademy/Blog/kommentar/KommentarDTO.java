package de.awacademy.Blog.kommentar;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class KommentarDTO {


    private String text;
    private long beitragId;

    /**
     * DTO zur Schützung der Datenbank
     * @param text Inhalt des DTO
     */
    public KommentarDTO(String text, long beitragId) {
        this.text = text;
        this.beitragId = beitragId;
    }

    public long getBeitragId() {
        return beitragId;
    }

    public void setBeitragId(long beitragId) {
        this.beitragId = beitragId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
