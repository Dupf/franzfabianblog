package de.awacademy.Blog.beitrag;

import javax.validation.constraints.Size;

public class BeitragDTO {

    @Size(min = 1, max = 50)
    private String ueberschrift;

    @Size(min = 1, max = 999)
    private String text;

    /**
     * DTO zur Schützung der Datenbank
     * @param ueberschrift Überschrift des DTO
     * @param text Inhalt des DTO
     */
    public BeitragDTO(String ueberschrift, String text) {
        this.ueberschrift = ueberschrift;
        this.text = text;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public void setText(String text) {
        this.text = text;
    }
}
