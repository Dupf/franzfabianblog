package de.awacademy.Blog.beitrag;

import de.awacademy.Blog.kommentar.Kommentar;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue
    private long id;

    private String ueberschrift;

    private String text;

    private Instant postedAt;

    @OneToMany(mappedBy = "beitrag")
    @OrderBy("postedAt asc")
    private List<Kommentar> kommentare;

    public Beitrag(){
    }

    /**
     * Beiträge erstellen, um Sie auf der Homepage anzuzeigen
     * @param ueberschrift Titel des Beitrags
     * @param text Inhalt des Beitrags
     * @param postedAt Zeitpunkt an dem der Beitrag verfasst wurde
     */
    public Beitrag(String ueberschrift, String text, Instant postedAt) {
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.postedAt = postedAt;
    }

    public void addKommentar( Kommentar kommentar){
        kommentare.add(kommentar);
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }
}
