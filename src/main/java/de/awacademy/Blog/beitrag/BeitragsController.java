package de.awacademy.Blog.beitrag;

import de.awacademy.Blog.kommentar.Kommentar;
import de.awacademy.Blog.kommentar.KommentarDTO;
import de.awacademy.Blog.kommentar.KommentarRepository;
import de.awacademy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@Controller
public class BeitragsController {

    BeitragsRepository beitragsRepository;
    KommentarRepository kommentarRepository;

    /**
     * Der Konstruktor der Klasse bekommt während der Programmlaufzeit den passenden Parameter übergeben
     * @param beitragsRepository
     * Der Parameter wird übergeben, damit wir mit Methodenaufrufen auf die Datenbank zugreifen können
     */
    @Autowired
    public BeitragsController(BeitragsRepository beitragsRepository, KommentarRepository kommentarRepository) {
        this.beitragsRepository = beitragsRepository;
        this.kommentarRepository = kommentarRepository;
    }

    /**
     * Diese Methode sorgt dafür, dass der Nutzer die Seite beitraege.html angezeigt bekommt
     * und über das Model werden die bisherigen Eintraege aus der Datenbank abgerufen
     * @param model gibt uns die Möglichkeit auf die Daten zuzugreifen
     * @return gibt einen String der auf die beitraege.html verweist zurück
     */
    @GetMapping("/beitraege")
    public String beitraege(@ModelAttribute("sessionNutzer") Nutzer nutzer, Model model){
        BeitragDTO beitragDTO = new BeitragDTO("", "");
        KommentarDTO kommentarDTO = new KommentarDTO("", 0);
        model.addAttribute("kommentar", kommentarDTO);
        model.addAttribute("beitraege", beitragsRepository.findAllByOrderByPostedAtDesc());
        model.addAttribute("beitragDTO", beitragDTO);
        return "beitraege";
    }

    /**
     * zeigt die Beiträge (neueste Zuerst) mit den jeweiligen Kommentaren (älteste zuerst)
     * @param nutzer der sessionNutzer
     * @param kommentarDTO das KommentarDTO beinhaltet die vom Nutzer eingegebenen Daten
     * @param beitrag der zum Kommentar zugehörige Beitrag wird für die Zuordnung übergeben
     * @param bindingResult checkt ob die Eingaben korrekt sind
     * @return gibt eine Fehlermeldung auf der Seite oder die aktuallisierte Seite zurück
     */
    @PostMapping("/beitraege")
    public String kommentieren(@ModelAttribute("sessionNutzer") Nutzer nutzer, @Valid @ModelAttribute("kommentar") KommentarDTO kommentarDTO, @ModelAttribute("beitrag") Beitrag beitrag, BindingResult bindingResult){
        // System.out.println(kommentarDTO.getBeitragId());
        if (bindingResult.hasErrors()){
            return "/beitraege";
        }
        kommentarRepository.save((new Kommentar(kommentarDTO.getText(), nutzer, Instant.now(), beitragsRepository.findById(kommentarDTO.getBeitragId()).get())));
        return "redirect:/beitraege";
    }

    /**
     * Beiträge werden nach der Erstellung in der Datenbank abgespeichert
     * @param nutzer gibt an welcher Nutzer den Beitrag geschrieben hat
     * @param beitragDTO enthält die zur Beitragserstellung nötigen INformationen
     * @return gibt die geupdatete Seite zurück
     */
    @PostMapping("/beitragErstellen")
    public String beitragErstellen(@ModelAttribute("sessionNutzer") Nutzer nutzer, @ModelAttribute("beitragDTO") BeitragDTO beitragDTO){
        if (nutzer.isAdmin()){
            beitragsRepository.save(new Beitrag(beitragDTO.getUeberschrift(), beitragDTO.getText(), Instant.now()));
        }
        return "redirect:/beitraege";
    }

    /**
     * Benutzer können ihre eigenen Kommentare löschen. Admins können alle Kommentare löschen
     * @param nutzer Eingeloggter Benutzer
     * @param kommentarId ID um das Kommentar zu identifizieren
     * @return leitet zu den Beiträgen weiter
     */
    @PostMapping("/kommentarLoeschen/{kommentarId}")
    public String kommentarLoeschen(@ModelAttribute("sessionNutzer") Nutzer nutzer, @PathVariable Long kommentarId){
        if (nutzer.getId() == kommentarRepository.findById(kommentarId).get().getNutzer().getId() || nutzer.isAdmin()) {
            kommentarRepository.deleteById(kommentarId);
        }
        return "redirect:/beitraege";
    }

    /**
     * Admins können Beiträge editieren
     * @param model das Model wird benötigt um der HTML Objekte zum Nutzen zu geben
     * @param beitragId BeitragsID um den richtigen Beitrag zu bearbeiten
     * @param nutzer    zur Prüfung auf den Admin-Boolean
     * @return gibt eine Seite zurück, auf der man einen Beitrag bearbeiten kann oder die Beitragsseite, wenn der Nutzer kein Admin ist
     */
    @PostMapping("/beitragBearbeiten/{beitragId}")
    public String beitragBearbeiten (Model model, @PathVariable long beitragId, @ModelAttribute ("sessionNutzer") Nutzer nutzer){
        if(nutzer.isAdmin()) {
            BeitragDTO beitragDTO = new BeitragDTO(beitragsRepository.findById(beitragId).orElseThrow().getUeberschrift(),
                    beitragsRepository.findById(beitragId).orElseThrow().getText());
            model.addAttribute("beitragZumBearbeiten", beitragDTO);
            model.addAttribute("beitrag", beitragsRepository.findById(beitragId).orElseThrow());
            return "beitragBearbeiten";
        }
        return "beitraege";
    }

    /**
     * Methode zum Speichern des geänderten Beitrags
     * @param beitragDTO    beinhaltet neuen Text und Überschrift
     * @param nutzer        muss weiterhin Admin sein
     * @param beitragId     wird von der Datenbank abgerufen
     * @return gibt einen Verweis auf die Beitragsseite zurück
     */
    @PostMapping("/beitragSpeichern/{beitragId}")
    public String beitragSpeichern(@ModelAttribute ("beitragZumBearbeiten") BeitragDTO beitragDTO,
                                   @ModelAttribute ("sessionNutzer") Nutzer nutzer,
                                   @PathVariable long beitragId){
        if (nutzer.isAdmin()){
            beitragsRepository.findById(beitragId).orElseThrow().setText(beitragDTO.getText());
            beitragsRepository.findById(beitragId).orElseThrow().setUeberschrift(beitragDTO.getUeberschrift());
            beitragsRepository.save(beitragsRepository.findById(beitragId).orElseThrow());
        }
        return "redirect:/beitraege";
    }

    /**
     * Hier werden die Beiträge mitallen Kommentaren gelöscht
     * @param nutzer Session Nutzer
     * @param beitragId sucht und Findet den Beitrag
     * @return leitet auf die Beitragsseite weiter
     */
    @PostMapping("/beitragLoeschen/{beitragId}")
    public String beitragLoeschen(@ModelAttribute("sessionNutzer") Nutzer nutzer, @PathVariable Long beitragId){
        if (nutzer.isAdmin()) {
            List<Kommentar> kommentare = kommentarRepository.findAllByBeitragId(beitragId);
            for (Kommentar kommentar : kommentare) {
                kommentarRepository.delete(kommentar);
            }
            beitragsRepository.deleteById(beitragId);
        }
        return "redirect:/beitraege";
    }
}