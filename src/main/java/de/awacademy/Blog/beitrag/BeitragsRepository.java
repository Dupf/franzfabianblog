package de.awacademy.Blog.beitrag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeitragsRepository extends CrudRepository<Beitrag, Long> {
    /**
     * Sucht alle Objekte aus der Datenbank
     * @return Beitragslsite
     */
    List<Beitrag> findAll();

    /**
     * Sucht alle Objekte aus der Datenbank (neueste zuerst)
     * @return Beitragsliste
     */
    List<Beitrag> findAllByOrderByPostedAtDesc();
}
