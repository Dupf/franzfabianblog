package de.awacademy.Blog.session;

import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.Optional;

public interface SessionRespository extends CrudRepository<Session, String> {

    /**Mit dieser Methode wird eine Session gesucht, welche durch die beiden Parameter näher definiert wird.
     *
     *
     * @param id Hier wird die Session ID übergeben nach der gesucht werden soll.
     * @param expiresAt Hier wird das Datum mit Zeit übergeben, an dem die Session abläuft.
     * @return Gibt eine Session wieder, die mit den beiden Parametern übereinstimmt, oder null
     */
    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt);
}
