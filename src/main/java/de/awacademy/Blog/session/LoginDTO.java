package de.awacademy.Blog.session;

public class LoginDTO {
    private String name;
    private String passwort;

    /**
     *  Das LoginDTO benötigt immer den Username und das Passwort
     *
     * @param username Name des Nutzers den er sich ausgesucht hat
     * @param passwort Passwort, welches er bei der Registrierung festgelegt hat.
     */
    public LoginDTO(String username, String passwort) {
        this.name = username;
        this.passwort = passwort;
    }

    public String getUsername() {
        return name;
    }

    public String getPasswort() {
        return passwort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }
}