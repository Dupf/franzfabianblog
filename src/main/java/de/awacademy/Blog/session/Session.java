package de.awacademy.Blog.session;

import de.awacademy.Blog.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private Nutzer nutzer;

    private Instant expiresAt;

    public Session() {
    }

    /**
     * Konstruktor, um die Session näher zu definieren
     * @param nutzer Welcher Nutzer ist gerade in einer Session
     * @param expiresAt Wann läuft die Session aus
     */
    public Session(Nutzer nutzer, Instant expiresAt) {
        this.nutzer = nutzer;
        this.expiresAt = expiresAt;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public String getId() {
        return id;
    }

    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public Instant getExpiresAt() {
        return expiresAt;
    }
}
