package de.awacademy.Blog.session;

import de.awacademy.Blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.Instant;
import java.util.Optional;

/**
 *
 */
@ControllerAdvice
public class SessionControllerAdvice {
    SessionRespository sessionRespository;

    /**
     * Dem Konstruktor wird die Session Repository übergeben, damit wir im weiteren Verlauf damit arbeiten können
     * @param sessionRespository Das Session Repository wird per Autowired zur Laufzeit automatisch übergeben
     */
    @Autowired
    public SessionControllerAdvice(SessionRespository sessionRespository) {
        this.sessionRespository = sessionRespository;
    }

    /**
     * Hier wird ein Nutzer erstellt, welcher von allen Controllern benutzt werden kann.
     * @param sessionId Hier wird die Session ID vom Cookie übergeben
     * @return es wird entweder ein Nutzer übergeben oder ein null-Objekt
     */
    @ModelAttribute("sessionNutzer")
    public Nutzer sessionNutzer(@CookieValue(value = "sessionId", defaultValue = "") String sessionId){
        if(!sessionId.isEmpty()){
            Optional<Session> optionalSession = sessionRespository.findByIdAndExpiresAtAfter(sessionId, Instant.now());
            if (optionalSession.isPresent()){
                Session session = optionalSession.get();
                session.setExpiresAt(Instant.now().plusSeconds(60*60));
                sessionRespository.save(session);
                return session.getNutzer();
            }
        }
        return null;
    }

}
