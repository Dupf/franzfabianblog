package de.awacademy.Blog.session;

import de.awacademy.Blog.nutzer.Nutzer;
import de.awacademy.Blog.nutzer.NutzerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.net.http.HttpResponse;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SessionController {

    SessionRespository sessionRespository;
    NutzerRepository nutzerRepository;

    /** Session und Nutzer Repositories werden für den Controller zum Nutzen im Kontruktor übergeben
     *
     * @param sessionRespository Datenbank für die Sessions
     * @param nutzerRepository  Datenbank für die Nutzer(user)
     */
    @Autowired
    public SessionController(SessionRespository sessionRespository ,NutzerRepository nutzerRepository) {
        this.sessionRespository = sessionRespository;
        this.nutzerRepository = nutzerRepository;
    }

    /**
     *  Für die Login-Page wird dem Model ein LoginDTO übergeben, welches dann auf der Seite gefüllt wird.
     * @param model Ist zwingend notwendig für die Logik
     * @return gibt einen Verweis auf die entsprechende HTML Seite zurück.
     */
    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("login", new LoginDTO("",""));
        return "login";
    }

    /**
     * In der folgenden Methode wird ein Nutzer angemeldet und es wird ihm eine neue Session zugewiesen.
     *
     * @param loginDTO LoginDTO welches in der GetMapping Methode erstellt und übergeben
     *                 wurde hat nun initialisierte Eigenschaften.
     * @param bindingResult überprüft den vorangestellten Parameter auf die von uns vorgegebene Gültigkeit
     * @param response Die Antwort, welche vom Server geschickt wird wird hier übergeben,
     *                 um ihr einen neuen Cookie geben zu können.
     * @return Bei Erfolg: Umleitung auf die Startseite
     *         Bei Fehlschlag: Fehlermeldung auf der gleichen Seite
     */
    @PostMapping("/login")
    public String login(@ModelAttribute("login") LoginDTO loginDTO, BindingResult bindingResult, HttpServletResponse response){
        Optional<Nutzer> optionalUser = nutzerRepository.findByNameAndPasswort(loginDTO.getUsername(), loginDTO.getPasswort());

        if(optionalUser.isPresent()){
            Session session = new Session(optionalUser.get(), Instant.now().plusSeconds(60*60));
            sessionRespository.save(session);

            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);
            return "redirect:/";
        }
        bindingResult.addError(new FieldError("login","passwort","Passwort stimmt nicht!"));
        return "login";
    }

    /** Logout Methode
     * Hier wird der Nutzer abgemeldet und seine Session aus der Session Datenbank gelöscht.
     *
     * @param sessionId Die zum Nutzer gehörige sessionID wird aus dem Cookie ausgelesen und der Methode übergeben.
     * @return leitet uns auf die Startseite zurück.
     */
    @PostMapping("/logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId) {
        Optional<Session> optionalSession = sessionRespository.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        optionalSession.ifPresent(session -> sessionRespository.delete(session));

        return "redirect:/";
    }
}
