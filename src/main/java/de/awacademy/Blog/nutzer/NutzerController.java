package de.awacademy.Blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NutzerController {

    NutzerRepository nutzerRepository;

    /**
     * der Nutzercontroller verwaltet die Benutzer in der Datenbank
     * @param nutzerRepository Datenbankverbindung
     */
    @Autowired
    public NutzerController(NutzerRepository nutzerRepository) {
        this.nutzerRepository = nutzerRepository;
    }

    /**
     * Mit Benutzernamen und Passwort kann man sich als Nutzer registrieren, um in der Datenbank zu erscheinen
     * @param model das Model ist zwingend notwendig für die Logik
     * @return gibt die registrierugns HTML zurück
     */
    @GetMapping("/registrate")
    public String zurRegistrierung(Model model) {
        model.addAttribute("registration", new NutzerDTO("", "", ""));
        return "registrate";
    }

    /**
     * erwartet ein Benutzernamen und zwei Passwörter zur Überprüfung und um den Benutzer in der Datenbank anlegen zu könen
     * @param registration Name + Passwort + bestätigtes Passwort
     * @param bindingResult Fehlerüberprüfung
     * @return leitet bei Erfolg auf die Login-Seite weier
     */
    @PostMapping("/registrate")
    public String registriert(@Valid @ModelAttribute("registration") NutzerDTO registration, BindingResult bindingResult) {
        if (!registration.getPasswort().equals(registration.getPasswort2())) {
            bindingResult.addError(new FieldError("registration", "passwort2", "Stimmt nicht überein"));
        }

        if (nutzerRepository.existsByName(registration.getName())) {
            bindingResult.addError(new FieldError("registration", "name", "Schon vergeben"));
        }

        if (bindingResult.hasErrors()) {
            return "registrate";
        }

        Nutzer user = new Nutzer(registration.getName(), registration.getPasswort());
        nutzerRepository.save(user);

        return "redirect:/login";
    }

    /**
     * Nutzerübersicht für die Admins um weitere Admins zu befördern
     * @param nutzer eingeloggter Benutzer muss Admin sein
     * @param model das Model ist zwingend notwendig für die Logik
     * @return gibt den Admins die Nutzerübersicht zurück oder leitet zum homegape weiter
     */
    @GetMapping("/nutzeruebersicht")
    public String nutzerUebersicht(@ModelAttribute("sessionNutzer") Nutzer nutzer, Model model) {
        if (nutzer != null) {
            if (nutzer.isAdmin()) {
                model.addAttribute("nutzers", nutzerRepository.findAll());
                return "nutzeruebersicht";
            }
        }
        return "redirect:/";
    }

    /**
     * erwartet eine Pathvariable nutzerId, um die admin variable auf true zu setzen
     * @param nutzer Ausführender Benutzer benötigt Admin-Rechte zur durchführung
     * @param nutzerId id des zu befördernden Benutzers für die Datenbanksuche
     * @return weierleitung zur Nutzerübersicht
     */
    @PostMapping("/zumAdminMachen/{nutzerId}")
    public String adminMachen(@ModelAttribute("sessionNutzer") Nutzer nutzer, @PathVariable Long nutzerId) {
        if (nutzer.isAdmin()) {
            Nutzer nutzerneu = nutzerRepository.findById(nutzerId).get();
            nutzerneu.setAdmin(true);
            nutzerRepository.save(nutzerneu);
        }
        return "redirect:/nutzeruebersicht";
    }
}