package de.awacademy.Blog.nutzer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class NutzerDTO {

    @NotBlank
    private String name;
    @Size(min =5)
    private String passwort;
    private String passwort2;

    /**
     * DTO zur Schützung der Datenbank
     * @param name Name des anzulegenden Nutzers
     * @param passwort Passworteingabe
     * @param passwort2 Passwort bestätigen
     */
    public NutzerDTO(@NotBlank String name, @Size(min = 5) String passwort, String passwort2) {
        this.name = name;
        this.passwort = passwort;
        this.passwort2 = passwort2;
    }



    public String getName() {
        return name;
    }


    public String getPasswort() {
        return passwort;
    }


    public String getPasswort2() {
        return passwort2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public void setPasswort2(String passwort2) {
        this.passwort2 = passwort2;
    }
}
