package de.awacademy.Blog.nutzer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NutzerRepository extends CrudRepository<Nutzer, Long> {
    List<Nutzer> findAll();

    /** Mit dieser Methode soll ein Nutzer gefunden werden, dessen Name und Passwort mit den übergebenen Parametern übereinstimmt.
     *
     * @param name Der Name nach dem gesucht werden soll
     * @param passwort Das Passwort nach dem gesucht werden soll
     * @return gibt einen Nutzer bei Erfolg und null bei Fehlschlag wieder
     */
    Optional<Nutzer> findByNameAndPasswort(String name, String passwort);

    /**
     *  Guckt ob der Name in der Datenbank vorhanden ist
     * @param name nach dem gesucht werden soll
     * @return gibt einen boolean zurück
     */
    boolean existsByName(String name);

    /**
     * Mit dieser Methode soll ein Nutzer gefunden und zurückgegeben werden.
     * @param name nach dem gesucht werden soll
     * @return einen Nutzer oder null (bei Fehlschlag)
     */
    Optional<Nutzer> findByName(String name);

}
