package de.awacademy.Blog.nutzer;

import de.awacademy.Blog.kommentar.Kommentar;

import javax.persistence.*;
import java.util.List;

@Entity
public class Nutzer {

    @Id
    @GeneratedValue
    private long id;
    private boolean admin;
    private String name;
    private String passwort;

    @OneToMany(mappedBy = "nutzer")
    private List<Kommentar> kommentare;


    public Nutzer() {
    }

    /**
     * Man kann sich als Benutzer anmelden, um Kommentare verfassen zu können
     * @param name Name des Benutzers
     * @param passwort Passwort des Benutzers
     */
    public Nutzer(String name, String passwort) {
        this.name = name;
        this.passwort = passwort;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }
}
