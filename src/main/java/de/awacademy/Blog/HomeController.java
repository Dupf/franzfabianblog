package de.awacademy.Blog;

import de.awacademy.Blog.nutzer.Nutzer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    /**
     * Mapping Methode die auf die Wurzelseite der Homepage leitet
     * @param nutzer Der "sessionNutzer" wird übergeben, damit man sich nicht neu anmelden muss, falls eine Session besteht
     * @return  Der Name der html-Datei wird zurückgegeben, welche wir über diese Methode angezeigt bekommen möchten
     */
    @GetMapping("/")
    public String index(@ModelAttribute("sessionNutzer")Nutzer nutzer){
        return "home";
    }
}
